let hours = 0;
let mins = 0;
let secs = 0;
let milis = 0;
let dihours = 0;
let dimins = 0;
let disecs = 0;
let dimilis = 0;
let stpw = document.querySelector(".time");
let status = "start";
let res = "stop";

let lapNow = null;
let my;
let rau = document.getElementById("lap");
let startbtn = document.querySelector(".start");
let lapbtn = document.querySelector(".lapb");
let stopbtn = document.querySelector(".stop");

startbtn.addEventListener("click", (e) => {
  if (status === "start" && res === "stop") {
    my = window.setInterval(st, 10);
    document.querySelector(".start").innerHTML = "Reset";
    status = "reset";
  } else if (status === "reset" && res === "resume") {
    document.querySelector(".stop").innerHTML = "Stop";
    res = "stop";
  } else if ((status = "reset" && res === "stop")) {
    window.clearInterval(my);
    reset();
    document.querySelector(".start").innerHTML = "Start";
    status = "start";
    window.clearInterval(my);
  }
});

stopbtn.addEventListener("click", (e) => {
  if (res === "stop" && status === "reset") {
    window.clearInterval(my);
    document.querySelector(".stop").innerHTML = "Resume";
    res = "resume";
  } else if (status === "reset" && res === "resume") {
    my = window.setInterval(st, 10);
    document.querySelector(".stop").innerHTML = "Stop";
    res = "stop";
  }
});

function st() {
  milis++;
  disecs = secs < 10 ? "0" + secs.toString() : secs;
  dimins = mins < 10 ? "0" + mins.toString() : mins;
  dihours = hours < 10 ? "0" + hours.toString() : hours;
  dimilis = milis < 10 ? "0" + milis.toString() : milis;
  if (milis / 100 === 1) {
    secs++;
    milis = 0;

    if (secs / 60 === 1) {
      mins++;
      secs = 0;

      if (mins / 60 === 1) {
        hours++;
        minutes = 0;
      }
    }
  }

  stpw.innerHTML = `${dihours} : ${dimins} : ${disecs} : ${dimilis}`;
}

function reset() {
  milis = 0;
  secs = 0;
  mins = 0;
  hours = 0;
  stpw.innerHTML = "00 : 00 : 00 : 00";
}

const structure = (number) => {
  return number <= 9 ? `0${number}` : number;
};

lapbtn.addEventListener("click",()=>{

 lapNow = `<div class="lap" style="background-color:#f2dc96;font-family:montserrat;border-radius:13px">${structure(
   hours
 )} : ${structure(mins)} : ${structure(secs)} : ${structure(milis)}</div><hr>`;
 rau.innerHTML += lapNow;

} );

